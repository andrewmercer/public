# Project homepage
* https://developer.hashicorp.com/vault/tutorials/getting-started/getting-started-install

# Usage

## Standalone installs

### Install on Debian-based distributions

```
ansible-playbook -bK -e "target=localhost" --tags "install,debian" ~/ansible/vault.yml
```

### Install on RHEL-based distributions

```
ansible-playbook -bK -e "target=localhost" --tags "install,rhel" ~/ansible/vault.yml
```

### Manually download binary

```
ansible-playbook -bK -e "target=localhost" --tags "install,manual" ~/ansible/vault.yml
```

## Clusters
todo

```
ansible-playbook -b -i ~/ansible/inventory/cluster_hosts.ini --extra-vars="target=cluster0" ~/ansible/vault.yml
```
