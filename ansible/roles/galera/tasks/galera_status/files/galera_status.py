#!/usr/bin/env python3

"""
Prerequisites:
grant all privileges on *.* to '[ db_user ]'@'[ client_ip ]' identified by '[ user_password ]'; # allow from a single client ip
or
grant all privileges on *.* to '[ db_user ]'@'[ subnet ].%' identified by '[ user_password ]'; # allow from anywhere on subnet
or
grant all privileges on *.* to '[ db_user ]'@'%' identified by '[ user_password ]'; # allow from anywhere
flush privileges;
"""

"""
Sample ini file:
[DEFAULT]
db1 = x.x.x.x
db2 = x.x.x.x
db3 = x.x.x.x
db_user = [ db_user ]
db_password = [ db_password ]
"""

"""
Online resources:
# http://galeracluster.com/documentation-webpages/monitoringthecluster.html
# https://www.w3schools.com/python/python_mysql_select.asp
# https://stackoverflow.com/questions/8884188/how-to-read-and-write-ini-file-with-python3
"""

"""
TODO:
- check that values aren't empty
- check that servers are online
- check that mysql connection is successful
"""


# python3 -m pip install mysql-connector --user

import argparse, mysql.connector
from configparser import ConfigParser

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--check', required=True, choices=["all", "wsrep_connected", "wsrep_local_state_comment", "wsrep_ready", "wsrep_cluster_conf_id", "wsrep_cluster_size", "wsrep_cluster_state_uuid", "wsrep_cluster_status"], help="Which cluster check would you like to see.")
args = parser.parse_args()

config = ConfigParser()
config.read('/etc/galera_status.ini')

db1 = config.get('DEFAULT', 'db1')
db2 = config.get('DEFAULT', 'db2')
db3 = config.get('DEFAULT', 'db3')
db_user = config.get('DEFAULT', 'db_user')
db_password = config.get('DEFAULT', 'db_password')

mydb1 = mysql.connector.connect(
  host = db1,
  user = db_user,
  password = db_password,
)

mydb2 = mysql.connector.connect(
  host = db1,
  user = db_user,
  password = db_password,
)

mydb3 = mysql.connector.connect(
  host = db1,
  user = db_user,
  password = db_password,
)

mycursor1 = mydb1.cursor()
mycursor2 = mydb2.cursor()
mycursor3 = mydb3.cursor()

cluster_check = args.check

def _wsrep_connected():

    """
    When the value is ON, the node has a network connection to one or more other nodes forming a cluster component.
    When the value is OFF, the node does not have a connection to any cluster components.
    """

    sql = 'SHOW STATUS LIKE "wsrep_connected";'

    mycursor1.execute(sql)
    mycursor2.execute(sql)
    mycursor3.execute(sql)

    myresult1 = mycursor1.fetchall()
    myresult2 = mycursor2.fetchall()
    myresult3 = mycursor3.fetchall()

    for x in myresult1, myresult2, myresult3:
      print(x)

    #if myresult1 == myresult2 == myresult3:
    #    print("[OK: wsrep_cluster_state_id is the same on all cluster nodes.]")

def _wsrep_local_state_comment():

    """
    When the node is part of the Primary Component, the typical return values are Joining, Waiting on SST, Joined, Synced or Donor.
    In the event that the node is part of a nonoperational component, the return value is Initialized.
    """

    sql = 'SHOW STATUS LIKE "wsrep_local_state_comment";'

    mycursor1.execute(sql)
    mycursor2.execute(sql)
    mycursor3.execute(sql)

    myresult1 = mycursor1.fetchall()
    myresult2 = mycursor2.fetchall()
    myresult3 = mycursor3.fetchall()

    for x in myresult1, myresult2, myresult3:
      print(x)

def _wsrep_ready():

    """
    When the node returns a value of ON it can accept write-sets from the cluster.
    When it returns the value OFF, almost all queries fail with the error: Error 1047 (08501) Unknown Command
    """

    sql = 'SHOW STATUS LIKE "wsrep_ready";'

    mycursor1.execute(sql)
    mycursor2.execute(sql)
    mycursor3.execute(sql)

    myresult1 = mycursor1.fetchall()
    myresult2 = mycursor2.fetchall()
    myresult3 = mycursor3.fetchall()

    for x in myresult1, myresult2, myresult3:
      print(x)

def _wsrep_cluster_conf_id():

    """
    Each node in the cluster should provide the same value.
    When a node carries a different, this indicates that the cluster is partitioned.
    """

    sql = 'SHOW STATUS LIKE "wsrep_cluster_conf_id";'

    mycursor1.execute(sql)
    mycursor2.execute(sql)
    mycursor3.execute(sql)

    myresult1 = mycursor1.fetchall()
    myresult2 = mycursor2.fetchall()
    myresult3 = mycursor3.fetchall()

    for x in myresult1, myresult2, myresult3:
      print(x)

    if myresult1 == myresult2 == myresult3:
        print("[OK: wsrep_cluster_conf_id is the same on all cluster nodes.]")

def _wsrep_cluster_size():

    """
    Shows the number of nodes in the cluster, which you can use to determine if any are missing.
    """

    sql = 'SHOW STATUS LIKE "wsrep_cluster_size";'

    mycursor1.execute(sql)
    mycursor2.execute(sql)
    mycursor3.execute(sql)

    myresult1 = mycursor1.fetchall()
    myresult2 = mycursor2.fetchall()
    myresult3 = mycursor3.fetchall()

    for x in myresult1, myresult2, myresult3:
      print(x)

def _wsrep_cluster_state_uuid():

    """
    Each node in the cluster should provide the same value.
    When a node carries a different value, this indicates that it is no longer connected to rest of the cluster.
    """

    sql = 'SHOW STATUS LIKE "wsrep_cluster_state_uuid";'

    mycursor1.execute(sql)
    mycursor2.execute(sql)
    mycursor3.execute(sql)

    myresult1 = mycursor1.fetchall()
    myresult2 = mycursor2.fetchall()
    myresult3 = mycursor3.fetchall()

    for x in myresult1, myresult2, myresult3:
      print(x)

    if myresult1 == myresult2 == myresult3:
        print("[OK: wsrep_cluster_state_id is the same on all cluster nodes.]")

def _wsrep_cluster_status():

    """
    The node should only return a value of Primary. Any other value indicates that the node is part of a nonoperational component.
    This occurs in cases of multiple membership changes that result in a loss of quorum or in cases of split-brain situations.
    """

    sql = 'SHOW STATUS LIKE "wsrep_cluster_status";'

    mycursor1.execute(sql)
    mycursor2.execute(sql)
    mycursor3.execute(sql)

    myresult1 = mycursor1.fetchall()
    myresult2 = mycursor2.fetchall()
    myresult3 = mycursor3.fetchall()

    for x in myresult1, myresult2, myresult3:
      print(x)

    #if myresult1 == "[('wsrep_cluster_status', 'Primary')]" and myresult1 == myresult2 == myresult3:
    #  print("OK")


if cluster_check == "all":
#if len(sys.argv) == 0:
#if len(sys.argv) > 1 or "all":
    _wsrep_connected()
    _wsrep_local_state_comment()
    _wsrep_ready()
    _wsrep_cluster_conf_id()
    _wsrep_cluster_size()
    _wsrep_cluster_state_uuid()
    _wsrep_cluster_status()

if cluster_check == "wsrep_connected":
    _wsrep_connected()

if cluster_check == "wsrep_local_state_comment":
    _wsrep_local_state_comment()

if cluster_check == "wsrep_ready":
    _wsrep_ready()

if cluster_check == "wsrep_cluster_conf_id":
    _wsrep_cluster_conf_id()

if cluster_check == "wsrep_cluster_size":
    _wsrep_cluster_size()

if cluster_check == "wsrep_cluster_state_uuid":
    _wsrep_cluster_state_uuid()

if cluster_check == "wsrep_cluster_status":
    _wsrep_cluster_status()
