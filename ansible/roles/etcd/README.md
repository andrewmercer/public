# Usage

## Install the standalone etcdctl binary localhost only

```
ansible-playbook -bK -e "target=localhost" --tags "etcdctl" ~/ansible/etcd.yml
```

## Create an etcd cluster

```
ansible-playbook -b -i ~/ansible/inventory/cluster_hosts.ini -e "target=cluster_1" --tags "install,cluster" ~/ansible/etcd.yml
```

# Bugs

## Use variable taken by '{{target}}' in Jinja template
Currently right now manually inserting the group in inventory that I know I will be using (same thing being done for galera role)

- templates/etcd.service.j2

```
{% for host in groups['cluster_1'] -%
```
