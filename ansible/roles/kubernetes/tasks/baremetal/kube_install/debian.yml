---

- name: major_version debug
  debug:
    msg: "{{ major_version }}"
- name: repo_version debug
  debug:
    msg: "{{ repo_version }}"

- name: "{{ ansible_distribution }}: Kubernetes node firewall ports"
  block:
    - name: "{{ ansible_distribution }}: Check if ufw is installed"
      shell: ufw status
      changed_when: false
      ignore_errors: true
      register: ufw_check

    - name: "{{ ansible_distribution }}: Enable ufw and initial configuration"
      ufw:
        #logging: on
        #policy: deny
        #state: enabled
        state: disabled
      when: ufw_check is succeeded

#    - name: "{{ ansible_distribution }}: open kubernetes ports"
#      ufw:
#        port: "{{ item }}"
#        proto: tcp
#        rule: allow
#      loop:
#        - 6443
#        - 2379:2380
#        - 10250
#        - 10251
#        - 10252
#        - 10255

- name: Cleanup old kubernetes repo files
  block:
    - name: find old repo files
      ansible.builtin.shell:
        cmd: find /etc/apt/sources.list.d -name 'kubernetes*.list' -exec rm {} \;
      #ansible.builtin.find:
      #  paths: /etc/apt/sources.list.d
      #  patterns: "kubernetes*.list"
      #  use_regex: true
      register: kubernetes_repo

    #- name: delete old repo files
    #  ansible.builtin.file:
    #    path: "{{ item.path }}"
    #    state: absent
    #  with_items: "{{ kubernetes_repo.files }}"

- name: "{{ ansible_distribution }}: Download and install kubernetes"
  block:
    - name: Install package dependencies required to download gpg key
      ansible.builtin.apt:
        name:
          - apt-transport-https
          - curl
        state: present

    #- name: Download the public signing key for the Kubernetes package repositories
    #  # (The same signing key is used for all repositories so you can disregard the version in the URL)
    #  ansible.builtin.shell:
    #    cmd: |
    #      curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key
    #      gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    #    creates: /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    
    - name: apt_key get
      ansible.builtin.get_url:
        dest: /tmp/Release.key
        url: "https://pkgs.k8s.io/core:/stable:/v{{ repo_version }}/deb/Release.key"

    - name: extract key data
      ansible.builtin.apt_key:
        keyring: /etc/apt/keyrings/kubernetes-apt-keyring.gpg
        url: "https://pkgs.k8s.io/core:/stable:/v{{ repo_version }}/deb/Release.key"
      
    - name: Create apt repository file
      ansible.builtin.copy:
        content: |
          deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v{{ repo_version }}/deb/ /
        dest: "/etc/apt/sources.list.d/kubernetes-{{ repo_version }}.list"

    - name: Update cache
      ansible.builtin.apt:
        update_cache: true

    #- name: apt_key get
    #  ansible.builtin.get_url:
    #    dest: /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    #    url: "https://pkgs.k8s.io/core:/stable:/v{{ repo_version }}/deb/Release.key"
    #- name: gpg dearmor
    #  ansible.builtin.shell:
    #    cmd: gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    #    creates: /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    #- name: apt_repository
    #  ansible.builtin.apt_repository:
    #    filename: kubernetes-{{ repo_version }}
    #    #repo: "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://apt.kubernetes.io/ {{ ansible_distribution_release }} main"
    #    repo: "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v{{ repo_version }}/deb/ /"
    #    state: present

    - name: Install ebtables
      ansible.builtin.apt:
        name: ebtables

    #- name: Install libxtables
      #ansible.builtin.apt:
        # What the actual fucking fuck.
        # fatal: [cluster0]: FAILED! =&gt; changed=false
        # msg: Unsupported parameters for (ansible.builtin.apt) module: allow_downgrade Supported parameters include: allow_unauthenticated, autoclean, autoremove, cache_valid_time, deb, default_release, dpkg_options, force, force_apt_get, install_recommends, only_upgrade, package, policy_rc_d, purge, state, update_cache, update_cache_retries, update_cache_retry_max_delay, upgrade
        # Documentation shows it should be there but ansible-doc doesn't have it
        #allow_downgrade: yes
        #name: libxtables12=1.8.2-4
        #state: present
      #ansible.builtin.shell:
      #  cmd: apt-get -y install libxtables12=1.8.2-4 --allow-downgrades

    #- name: Install remaining dependencies
    #  ansible.builtin.apt:
    #    name:
    #      - iptables
    #      - netbase
    #      - python3-dnspython
    #      - ufw


    - name: "{{ ansible_distribution }}: Check if packages are being held"
      ansible.builtin.shell: apt-mark showhold
      changed_when: false
      register: held_packages

    - name: "{{ ansible_distribution }}: Unhold this package version if held"
      dpkg_selections:
        name: "{{ item }}"
        selection: install
      when: held_packages.stdout | length > 0
      with_items:
        - kubeadm
        - kubectl
        - kubelet

    - name: "{{ ansible_distribution }}: Register installed apt_version of kubernetes packages"
      ansible.builtin.shell: dpkg -l | grep -i kubeadm | awk '{print $3 }' | uniq
      register: installed_apt_version

    - name: "{{ ansible_distribution }}: Delete the kubernetes packages if mismatch between desired and installed versions"
      ansible.builtin.apt:
        name: "{{ item }}"
        state: absent
      when: apt_version != installed_apt_version
      with_items:
        - kubeadm
        - kubectl
        - kubelet

    - name: "{{ ansible_distribution }}: {{ role_name }}: Install kubelet kubeadm kubectl"
      ansible.builtin.apt:
        # allow_downgrade: yes # this parameter is not available in ansible=2.9
        # but when I use a newer version via venv, it errors about missing the modprobe module
        # https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html
        name:
          - kubeadm={{ apt_version }}
          - kubectl={{ apt_version }}
          - kubelet={{ apt_version }}
        update_cache: yes
  
    - name: Start and enable kubelet
      ansible.builtin.systemd:
        enabled: true
        masked: false
        name: kubelet.service
        state: started

    - name: Hold this package version
      dpkg_selections:
        name: "{{ item }}"
        selection: hold
      with_items:
        - kubeadm
        - kubectl
        - kubelet
