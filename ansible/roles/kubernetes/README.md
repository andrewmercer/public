# Usage

## Baremetal

### Brand new baremetal cluster - install packages and initialize new cluster

```
ansible-playbook -b -i ~/ansible/inventory/cluster_hosts.ini -e "target=cluster_1 controller=cluster0" --tags "baremetal,containerd,install,init,join" ~/ansible/kubernetes.yml
```

### Init

#### 1 control plane (cp) node, all others are workers

```
ansible-playbook -b -i ~/ansible/inventory/cluster_hosts.ini -e "target=cluster_1 controller=cluster0" --tags "baremetal,init,join" ~/ansible/kubernetes.yml
```

### Join worker nodes to cluster

```
ansible-playbook -b -i ~/ansible/inventory/cluster_hosts.ini -e "target=cluster_1 controller=cluster0" --tags "baremetal,join" ~/ansible/kubernetes.yml
```

### Reset cluster

- $HOME/ansible/roles/kubernetes/tasks/baremetal/kube_reset/main.yml

```
ansible-playbook -b -i ~/ansible/inventory/cluster_hosts.ini -e "target=cluster_1 controller=cluster0" --tags "baremetal,reset" ~/ansible/kubernetes.yml
```

### Manually set k8s version, networking options

```
ansible-playbook -b -i ~/ansible/inventory/cluster_hosts.ini -e "target=cluster_1 controller=cluster0" \
-e "runtime=containerd version=1.21.1-00 major_version=1.21.1 ipv4_pool_cidr=10.244.0.0/16" --tags "init" ~/ansible/kubernetes.yaml
```

## kube-bench
* https://github.com/aquasecurity/kube_bench/blob/main/docs/installation.md

# Bugs

## this section doesn't appear to work

```
TASK [kubernetes : Debian: Unhold this package version if held] ****************************************************************************************************************************************************
changed: [cluster2] => (item=kubeadm)
changed: [cluster0] => (item=kubeadm)
changed: [cluster1] => (item=kubeadm)
changed: [cluster1] => (item=kubectl)
changed: [cluster0] => (item=kubectl)
changed: [cluster2] => (item=kubectl)
changed: [cluster1] => (item=kubelet)
changed: [cluster0] => (item=kubelet)
changed: [cluster2] => (item=kubelet)

TASK [kubernetes : Debian: Register installed apt_version of kubernetes packages] **********************************************************************************************************************************
changed: [cluster0]
changed: [cluster2]
changed: [cluster1]

TASK [kubernetes : Debian: Delete the kubernetes packages if mismatch between desired and installed versions] ******************************************************************************************************
changed: [cluster0] => (item=kubeadm)
changed: [cluster1] => (item=kubeadm)
changed: [cluster2] => (item=kubeadm)
changed: [cluster2] => (item=kubectl)
changed: [cluster1] => (item=kubectl)
changed: [cluster0] => (item=kubectl)
changed: [cluster1] => (item=kubelet)
changed: [cluster2] => (item=kubelet)
changed: [cluster0] => (item=kubelet)
```
