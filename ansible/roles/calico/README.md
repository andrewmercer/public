# Binary

```
ansible-playbook -i ~/ansible/inventory/cluster_hosts.ini -b -e "target=cluster_1 controller=cluster0" --tags "calicoctl,binary" ~/ansible/calico.yaml
```

# Plugin

```
ansible-playbook -i ~/ansible/inventory/cluster_hosts.ini -b -e "target=cluster_1 controller=cluster0" --tags "calicoctl,plugin" ~/ansible/calico.yaml
```

# Container

```
ansible-playbook -i ~/ansible/inventory/cluster_hosts.ini -b -e "target=cluster_1 controller=cluster0" --tags "calicoctl,container" ~/ansible/calico.yaml
```

# Pod

```
ansible-playbook -i ~/ansible/inventory/cluster_hosts.ini -b -e "target=cluster_1 controller=cluster0" --tags "calicoctl,pod" ~/ansible/calico.yaml
```
