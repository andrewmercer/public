# docker_install module

* https://docs.docker.com/engine/install/debian/#install-using-the-repository
* https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository

## Usage

```
ansible-playbook -i ~/ansible/inventory/cluster_hosts.ini -b -e "target=cluster_1" --tags "install" ~/ansible/docker.yml
```

# docker_uninstall module

## Usage

```
ansible-playbook -i ~/ansible/inventory/cluster_hosts.ini -b -e "target=cluster_1" --tags "uninstall" ~/ansible/docker.yml
```

# docker_swarm module
* https://docs.docker.com/engine/swarm/swarm-tutorial

## Usage

### Brand new cluster - install packages and initialize new cluster

```
ansible-playbook -b -i ~/ansible/inventory/cluster_hosts.ini -b -e "target=cluster_1 manager=cluster0" --tags "install,swarm" ~/ansible/docker.yml
```

# docker_compose module

* https://docs.docker.com/compose/install

## user install

```
ansible-playbook --extra-vars="target=localhost" --tags "compose,user"~/ansible/docker.yml
```

## global install

```
ansible-playbook -bK --extra-vars="target=localhost" --tags "compose,global"~/ansible/docker.yml
```
