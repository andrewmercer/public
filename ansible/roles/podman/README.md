* https://developers.redhat.com/blog/2020/09/25/rootless-containers-with-podman-the-basics#example__using_rootless_containers

# Usage

```
ansible-playbook --become --extra-vars="target=localhost" ~/ansible/podman.yaml
```

# podman_compose
* https://github.com/containers/podman-compose
* https://www.redhat.com/sysadmin/podman-compose-docker-compose
