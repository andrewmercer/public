*NOTE*: currently a work in progress

# Project page
* https://www.ansible.com

# Playbooks and roles

## Playbooks
* https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html

Playbooks are a completely different way to use ansible than in ad-hoc task execution mode, and are particularly powerful.

## Roles
* https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html

Roles are ways of automatically loading certain vars_files, tasks, and handlers based on a known file structure. Grouping content by roles also allows easy sharing of roles with other users.
Roles expect files to be in certain directory names. Roles must include at least one of these directories, however it is perfectly fine to exclude any which are not being used. When in use, each directory must contain a main.yml file, which contains the relevant content:

    tasks - contains the main list of tasks to be executed by the role.
    handlers - contains handlers, which may be used by this role or even anywhere outside this role.
    defaults - default variables for the role (see Using Variables for more information).
    vars - other variables for the role (see Using Variables for more information).
    files - contains files which can be deployed via this role.
    templates - contains templates which can be deployed via this role.
    meta - defines some meta data for this role. See below for more details.

### Re-usable roles

- etc_hosts # populates /etc/hosts file with cluster members
- external_ip # retrieves external ip 

# Handlers

## Global handlers workaround
Write reusable handlers that all roles can use

### Create a role named handlers

- roles/handlers

#### Include the handlers role in the role's dependencies

- roles/[ my_role ]/meta/main.yml

```
dependencies:
  - handlers
```

### Simply write a notify in your role's task
For example:

```
- name: Download systemd service unit file
  ansible.builtin.get_url:
    dest: /etc/systemd/system
    url: https://raw.githubusercontent.com/containerd/containerd/main/containerd.service
  **notify: systemd daemon-reload**
```

# Variables (vars)

## Global variables

- $HOME/ansiblel/group_vars/all/main.yml

# Inventory

# Controlling execution flow in Ansible

## Conditionals
* https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_conditionals.html

## Loops
* https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_loops.html

## Delegation
* https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_delegation.html

## Error handling

## Debugging
* Debug mode 

## Variables

# Building custom modules
- todo
