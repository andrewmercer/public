# Usage

```
terraform apply -var="name=[ bucket_name ]" -var="project_id" -var="region=us-east-1"
```

```
terraform apply -var="location=us-east-1" -var="name=www.andrewmercer.net" -var="project_id=andrewmercer-net-314903"
```

# General resources

## Terraform support for cloud storage
* https://cloud.google.com/storage/docs/terraform-for-cloud-storage

## Terraform simple bucket example
* https://github.com/terraform-google-modules/terraform-google-cloud-storage/tree/master/examples/simple_bucket

## IAM policy for Cloud Storage Bucket
* https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam

## Host a static website on Google Cloud doc
* https://cloud.google.com/storage/docs/hosting-static-website

- Create the bucket
- Public access
  * https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_access_control
- Assign speciality pages
- Load balancer
  - ssl
  - frontend
  - backend
- Output DNS information for DNS provider
