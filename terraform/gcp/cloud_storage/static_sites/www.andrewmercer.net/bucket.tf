resource "google_storage_bucket" "static_website" {
  name				= var.name
  location			= var.location
  #force_destroy			= var.force_destroy
  public_access_prevention	= var.public_access_prevention
}

resource "google_storage_bucket_access_control" "public_rule" {
  bucket = var.name
  role   = "READER"
  entity = "allUsers"
}
