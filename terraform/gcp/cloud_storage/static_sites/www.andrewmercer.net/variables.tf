variable "autoclass" {
  description = "While set to true, autoclass is enabled for this bucket."
  type        = bool
  default     = false
}

variable "bucket_policy_only" {
  description = "Enables Bucket Policy Only access to a bucket."
  type        = bool
  default     = true
}

variable "encryption" {
  description = "A Cloud KMS key that will be used to encrypt objects inserted into this bucket"
  type = object({
    default_kms_key_name = string
  })
  default = null
}

variable "labels" {
  description = "A set of key/value label pairs to assign to the bucket."
  type        = map(string)
  default     = null
}

variable "location" {
  description	= "Location of the bucket"
  type		= string
}

variable "name" {
  description	= "The bucket name"
  type		= string
}

variable "project_id" {
  description	= "The project id"
  type		= string
}

variable "public_access_prevention" {
  description = "Prevents public access to a bucket. Acceptable values are inherited or enforced. If inherited, the bucket uses public access prevention, only if the bucket is subject to the public access prevention organization policy constraint."
  type        = string
  default     = "disabled"
}

variable "storage_class" {
  description = "The Storage Class of the new bucket."
  type        = string
  default     = "standard"
}

variable "versioning" {
  default	= true
  description	= "While set to true, versioning is fully enabled for this bucket."
  type		= bool
}
