variable "owner" {
  type = string
  default = "Andrew Mercer"
}

variable "project-name" {
  type = string
  default = "geoproximity"
}

variable "default-tags" {
  type = map(string)
  default = {
    app_env = "dev"
    app_name = "geoproximity web app"
    owner = "Andrew Mercer"
  }
}

variable "region" {
  default = "us-east-1"
  description = "Region"
  type = string
}

variable "vpc-cidr" {
  type = string
  default = "10.0.0.0/16"
}

variable "subnet1" {
  type = string
  default = "10.0.1.0/24"
}

variable "subnet2" {
  type = string
  default = "10.0.2.0/24"
}

variable "subnet3" {
  type = string
  default = "10.0.3.0/24"
}

variable "subnet4" {
  type = string
  default = "10.0.4.0/24"
}

#variable "subnet_ids" {
#  description = "Subnet IDs for EC2 instances"
#  type        = list(string)
#}

variable "geoproximity-allow" {
  type = string
  default = "allow"
}

#variable "externalip" {
#  type = program
#  default = "external_ip.py"
#}

variable "ssh-key-name" {
  type = string
  default = "amercer"
}

variable "instance-type" {
  type = string
  default = "t2.micro"
}

variable "instance-name" {
  type = string
  default = "geoproximity_ami"
}

variable "instance-ami" {
  type = string
  default = "ami-053b0d53c279acc90" # ubuntu ami id us-east-1
}

/* For reference for now
variable "custom_ami" {
  type = string
  default =  ami-07ca69143dca2ee30
}
*/

variable "disk-size" {
  type = number
  default = 8
}

variable "disk-type" {
  type = string
  default = "gp2"
}

variable "disk-name" {
  type = string
  default = "/dev/sda1"
}
