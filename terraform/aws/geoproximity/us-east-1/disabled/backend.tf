terraform {
  backend "s3" {
    bucket         = "terraform-backend-amercer"
    region         = "us-east-1"
    dynamodb_table = "backend-dynamodb-table"
  }
}
