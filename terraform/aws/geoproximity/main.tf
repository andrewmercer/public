terraform {
  backend "local" {
    path = "terraform.tfstate"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

module "ca-central-1" {
  source = "./ca-central-1"
}

module "eu-central-1" {
  source = "./eu-central-1"
}

module "us-east-1" {
  source = "./us-east-1"
}
