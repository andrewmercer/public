output "vpc_id" {
  value = "aws_vpc.${var.project-name}_vpc.id"
}

output "subnet_id" {
  value = "aws_subnet..id"
}

output "ec2instance" {
  value = "aws_instance.${var.instance-name}.public_ip"
}

output "geoprox_ami" {
  value = "aws_ami.id"
}
