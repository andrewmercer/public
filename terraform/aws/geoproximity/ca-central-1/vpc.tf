# Create the VPCs

## Create a VPC and related resources in region 1

resource "aws_vpc" "this" {
  cidr_block           = var.vpc-cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = merge(var.default-tags, {
    Name = "vpc-${var.project-name}"
  })
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.this.id

  tags = merge(var.default-tags, {
    Name = "gw-${var.project-name}"
  })
}

data "aws_availability_zones" "list" {
  state = "available"
}

resource "aws_subnet" "subnet1" {
  cidr_block        = var.subnet1
  vpc_id            = aws_vpc.this.id
  availability_zone = data.aws_availability_zones.list.names[0]

  tags = merge(var.default-tags, {
    Name = "${var.project-name}-subnet1"
  })
}

resource "aws_subnet" "subnet2" {
  cidr_block        = var.subnet2
  vpc_id            = aws_vpc.this.id
  availability_zone = data.aws_availability_zones.list.names[1]

  tags = merge(var.default-tags, {
    Name = "${var.project-name}-subnet2"
  })
}

/* There are only 2 availability zones in ca-central-1

resource "aws_subnet" "subnet3" {
  cidr_block        = var.subnet3
  vpc_id            = aws_vpc.this.id
  availability_zone = data.aws_availability_zones.list.names[0]

  tags = merge(var.default-tags, {
    Name = "${var.project-name}-subnet3"
  })
}

resource "aws_subnet" "subnet4" {
  cidr_block        = var.subnet4
  vpc_id            = aws_vpc.this.id
  availability_zone = data.aws_availability_zones.list.names[0]

  tags = merge(var.default-tags, {
    Name = "${var.project-name}-subnet4"
  })
}

*/

resource "aws_route_table" "subnet1-rt" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = merge(var.default-tags, {
    Name = "${var.project-name}-subnet1-rt"
  })
}

resource "aws_route_table" "subnet2-rt" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = merge(var.default-tags, {
    Name = "${var.project-name}-subnet2-rt"
  })
}

/* There are only 2 availability zones in ca-central-1

resource "aws_route_table" "subnet3-rt" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = merge(var.default-tags, {
    Name = "${var.project-name}-subnet3-rt"
  })
}

resource "aws_route_table" "subnet4-rt" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = merge(var.default-tags, {
    Name = "${var.project-name}-subnet4-rt"
  })
}

*/

resource "aws_route_table_association" "route-assoc1" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.subnet1-rt.id
}

resource "aws_route_table_association" "route-assoc2" {
  subnet_id      = aws_subnet.subnet2.id
  route_table_id = aws_route_table.subnet2-rt.id
}

/* There are only 2 availability zones in ca-central-1

resource "aws_route_table_association" "route-assoc3" {
  subnet_id      = aws_subnet.subnet3.id
  route_table_id = aws_route_table.subnet3-rt.id
}

resource "aws_route_table_association" "route-assoc4" {
  subnet_id      = aws_subnet.subnet4.id
  route_table_id = aws_route_table.subnet4-rt.id
}

*/

/* Use this in the future to limit security group access to my IP
data "external" "external_ip" {
  program = [ "python", "external_ip.py" ]
}

*/

resource "aws_security_group" "geoproximity-allow" {

  name        = "geoproximity-allow"
  description = "Allow what we need"
  vpc_id      = aws_vpc.this.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    #cidr_blocks = ["${data.external.external_ip.result.myip}/32", cidrsubnet(var.vpc-cidr, 2, 1)]
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    #cidr_blocks = ["${data.external.external_ip.result.myip}/32", cidrsubnet(var.vpc-cidr, 2, 1)]
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    #cidr_blocks = ["${data.external.external_ip.result.myip}/32", cidrsubnet(var.vpc-cidr, 2, 1)]
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${var.project-name}-${var.geoproximity-allow}"
  }
}
