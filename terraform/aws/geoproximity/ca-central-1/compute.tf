# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ami_from_instance

# Get latest Ubuntu Linux Focal Fossa 20.04 AMI
data "aws_ami" "ubuntu-linux-2004" {
  most_recent = true
  owners      = ["099720109477"] # Canonical
 
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-*"]
  }
 
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "geoproximity_temp_instance" {

  ami = data.aws_ami.ubuntu-linux-2004.id
  count = 1
  instance_type = var.instance-type
  #for_each = data.aws_subnet_ids.private.ids
  #subnet_id = each.value
  #subnet_id = var.subnet_ids[count.index % length(var.subnet_ids)]
  #subnet_id = "${var.subnet1}.id"
  #subnet_id = data.terraform_remote_state.vpc.outputs.private_subnet_ids[count.index % length(data.terraform_remote_state.vpc.outputs.private_subnet_ids)]
  #subnet_id = "${element(data.aws_subnet_ids.private.ids, count.index)}"
  subnet_id = aws_subnet.subnet1.id
  associate_public_ip_address = true
  key_name = var.ssh-key-name
  vpc_security_group_ids = [aws_security_group.geoproximity-allow.id]

  user_data = <<-EOF
    #!/bin/bash
    apt-get -y update
    apt-get -y install nginx
  EOF

  #depends_on = [ "aws_security_group.geoproximity-allow" ]

  root_block_device {
    delete_on_termination = true
    volume_size = 8
    volume_type = "gp2"
  }
  tags = {
    Name = var.instance-name
  }

}
