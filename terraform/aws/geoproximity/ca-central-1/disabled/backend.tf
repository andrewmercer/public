terraform {
  backend "s3" {
    bucket         = "terraform-backend-amercer"
    region         = "ca-central-1"
    dynamodb_table = "backend-dynamodb-table"
  }
}
