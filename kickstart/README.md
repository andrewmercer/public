# Serve over http on a local network

## Using python's builtin web server on the cli

```
cd ~/kickstart && \
python3 -m http.server &
or
python -m SimpleHTTPServer in python2 &
```

## Using container

```
docker-compose up -d

or

podman compose up -d 
```

## Using Kubernetes
- todo, have manifests, but need to test

# Sample Anaconda ks options at the linux kernel cli
* https://anaconda-installer.readthedocs.io/en/latest/boot-options.html#network-options

```
ks=http://kickstart_host:8000/kickstart_ks.cfg [ networking_options ] net.ifnames=0 biosdevnames=0
```

## Basic networking options
```
ks=http://kickstart_host:8000/kickstart_ks.cfg ip=dhcp nameserver=192.168.0.1 net.ifnames=0 biosdevnames=0
```

## Advanced networking options
```
ks=http://kickstart_host:8000/kickstart_ks.cfg ip=192.168.0.100::192.168.0.1:255.255.255.0:mydesktop:eth0:none nameserver=192.168.0.1 net.ifnames=0 biosdevnames=0
```

### Advanced networking options explained
```
ip=<ip>::<gateway>:<netmask>:<hostname>:<interface>:none nameserver=<nameserver_ip>
```

# To add as an option when creating a KVM instance using virt-install:

```
virt-install --name [ vm_name ] --description [ vm_description ] --memory=[memory] --vcpus=[vcpus] \
--location [ iso_path ] --os-type=[os_type] --os-variant=[os_variant] --disk pool=default,bus=virtio,size=[disk_size] \
--network bridge=[bridge_name],model=virtio --autostart --graphics none --console=pty,target_type=serial \
--extra-args "console=ttyS0,115200n8 inst.sshd ks=http://[ server_ip ]:8000/[kickstart_file].cfg ip=dhcp"
```

## Example

```
virt-install --name controller0 --description "OpenStack Controller0" \
--memory=16384 --vcpus=8 \
--location ~/Downloads/CentOS-7-x86_64-Minimal-1810.iso \
--os-type=linux --os-variant=rhel7 \
--disk pool=default,bus=virtio,size=50 \
--network bridge=virbr0,model=virtio \
--autostart --graphics none --console=pty,target_type=serial \
--extra-args "console=ttyS0,115200n8 inst.sshd ks=http://192.168.0.100:8000/standard_rhel8_virt_ks.cfg ip=dhcp"
```

# Generate a Linux password hash

```
echo "Password123!" | mkpasswd --rounds=656000 --method=SHA-512 --stdin
```
