# Serve over http on a local network

## Using python's builtin web server on the cli

```
cd ~/kickstart && \
python3 -m http.server &
or
python -m SimpleHTTPServer in python2 &
```

## Using container

```
docker-compose up -d

or

podman compose up -d 
```
