#!/bin/bash

PUB_ROOT=$HOME/public

# ansible
ANSIBLE_PUB_ROOT=$PUB_ROOT/ansible
ANSIBLE_PUB_ROLES=$ANSIBLE_PUB_ROOT/roles
ANSIBLE_ROOT=$HOME/ansible
ANSIBLE_ROLES=$ANSIBLE_ROOT/roles
ANSIBLE_README=$ANSIBLE_ROOT/README.md
ANSIBLE_ROLE=( "calico" "containerd" "crio" "docker" "etcd" "galera" "golang" "handlers" "helm" "hugo" "kubernetes" "podman" "straight_facts" "terraform" "vault" )

for ansible_role in "${ANSIBLE_ROLE[@]}"; do
  rsync -av $ANSIBLE_ROLES/$ansible_role $ANSIBLE_PUB_ROLES
done
for ansible_role in "${ANSIBLE_ROLE[@]}"; do
  rsync -av ${ANSIBLE_ROOT}/$ansible_role.yml $ANSIBLE_PUB_ROOT
done

rsync -av "${ANSIBLE_ROOT}"/group_vars "${ANSIBLE_PUB_ROOT}"
rsync -av "${ANSIBLE_README}" "${ANSIBLE_PUB_ROOT}"

# helm

# packer

# python
PY_ROOT=$HOME/python/bin
PYTHON_FILES=( "ansible-create-role.py" "external_ip.py" "pizza_chooser.py" )
for p in "${PYTHON_FILES[@]}"; do
  rsync -av $PY_ROOT/$p $PUB_ROOT/python
done

# terraform
TERRAFORM_PUB_ROOT=$PUB_ROOT/terraform
#TERRAFORM_ROLES=( "aws" "gcp" "libvirt" )
rsync --exclude '.terraform*' --exclude 'amis.txt' -av $HOME/terraform/aws/compute/geoproximity $TERRAFORM_PUB_ROOT/aws
rsync --exclude '.terraform*' -av $HOME/terraform/gcp/cloud_storage $TERRAFORM_PUB_ROOT/gcp
