#!/usr/bin/python3

# -*- coding: utf-8 -*-  
import argparse
import os
#import pathlib
from pathlib import Path
import shutil
import sys

"""

 Syntax notes:

 - Formatted string literals
  - https://docs.python.org/3/whatsnew/3.6.html#pep-498-formatted-string-literals
   - example: roles_dir = f"{root_dir}/roles"

"""

"""

 Future improvements:

 - add support for distro detection i.e. create a tasks/main.yml entrypoint that detects and routes to one of tasks/{rhel,debian}.yml
 - create a class for a standalone role outside the ~/ansible directory structure
 - check if the role name starts with "ansible" and if so print error message and exit
 - organize into classes
 - populate meta/main.yml the same way ansible-galaxy does (or similar)
 - run yml lint on the syntax on check-in

"""

# argument parser

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--role', required=True, help="Enter the role name.")
parser.add_argument('-d', '--delete', required=False, help="Delete a role.", action="store_true") # https://docs.python.org/3/howto/argparse.html#introducing-optional-arguments
args = parser.parse_args()

"""

 set the home and root_dir variables
 this is the path to the ansible code
 which in this case is within the user's home directory
 i.e. /home/[ user_name ]/ansible

 NOTE:

 # I used to have ansible_public & ansible_private repos, but now it's just ~/ansible
 #repo = args.repo
 # https://stackoverflow.com/questions/46119144/printing-variable-that-contains-string-and-2-other-variables
 #root_dir = f"{home}/ansible_{repo}"
 # https://stackoverflow.com/questions/4028904/how-to-get-the-home-directory-in-python

"""

# global vars
home = str(Path.home())
root_dir = f"{home}/ansible"

"""

 root_dir

 Check if root_dir path exists - if not, create it
 This is the root ansible directory in the user's home directory
 i.e. ~/ansible

"""

if not os.path.exists(root_dir):
    os.mkdir(root_dir)

"""

 roles_dir

 Check if roles_dir path exists - if not, create it
 This is the root roles directory i.e. $root_dir/plays
 a.k.a ~/ansible/roles

"""

roles_dir = f"{root_dir}/roles"
if not os.path.exists(roles_dir):
    os.mkdir(roles_dir)

"""

 plays_dir

 Check if plays_dir path exists - if not, create it
 This is the root plays directory i.e. $root_dir/plays
 a.k.a. ~/ansible/plays

"""

plays_dir = f"{root_dir}/plays"
if not os.path.exists(plays_dir):
    os.mkdir(plays_dir)

"""

 role and role_dir

 Take the 'role' variable from arguments and create
 the role directory in $root_dir/roles/[ role_name ]
 a.k.a. ~/ansible/roles/[ role_name ]

"""

role = args.role
role_dir = f"{roles_dir}/{role}"
if not os.path.exists(role_dir):
    os.mkdir(role_dir)


def create_role():

    # Create main [ role_name ].yml file and populate with content

    role_yml = f"{root_dir}/{role}.yml"
    if not os.path.exists(role_yml):
        Path(role_yml).touch()
        filename = role_yml
        f = open(filename, "w")
        f.write("--- \n")
        f.write("- hosts: '{{target}}' \n")
        f.write("  # pre_tasks: \n")
        f.write("  roles: \n")
        # https://stackoverflow.com/questions/899103/writing-a-list-to-a-file-with-python
        f.write("    - %s\n" % role)
        f.write("  # post_tasks: \n")
        f.write("  # tags:")
        f.close()

    # Create a .gitignore file in the root of the role_dir and populate with default content

    role_gitignore = f"{role_dir}/.gitignore"
    if not os.path.exists(role_gitignore):
        Path(role_gitignore).touch()
        filename = role_gitignore
        f = open(filename, "w")
        f.writelines([ "*.swp\n", "*.retry\n" ])
        #f.write("*.swp\n")
        #f.write("*.retry")
        f.close()

    # Create a .ymllint file in the root of the role_dir and populate with content

    role_ymllint = f"{role_dir}/.ymllint"
    if not os.path.exists(role_ymllint):
        Path(role_ymllint).touch()
        filename = role_ymllint
        f = open(filename, "w")
        f.writelines([ "---\n",
                       "# Based on ansible-lint config\n",
                       "extends: default\n\n",
                       "rules:\n",
                       "  braces:\n",
                       "    max-spaces-inside: 1\n",
                       "    level: error\n",
                       "  brackets:\n",
                       "    max-spaces-inside: 1\n",
                       "    level: error\n",
                       "  colons:\n",
                       "    max-spaces-after: -1\n",
                       "    level: error\n",
                       "  commas:\n"
                       "    max-spaces-after: -1\n",
                       "    level: error\n",
                       "  comments: disable\n",
                       "  comments-indentation: disable\n",
                           "  document-start: disable\n",
                       "  empty-lines:\n",
                       "    max: 3\n",
                       "    level: error\n",
                       "  hyphens:\n",
                       "    level: error\n",
                       "  indentation: disable\n",
                       "  key-duplicates: enable\n",
                           "  line-length: disable\n",
                       "  new-line-at-end-of-file: disable\n",
                       "  new-lines:\n",
                       "    type: unix\n",
                       "  trailing-spaces: disable\n",
                       "  truthy: disable"
    ])
        f.close()


    # Create the role_files files in the new role root directory

    role_files = [ 'README.md' ]
    for files in role_files:
        r_role_files = os.path.join(role_dir,str(files))
        if not os.path.exists(r_role_files):
            Path(r_role_files).touch()

    # A multi-line list of all the directories needed inside each role

    default_dirs = [
     'defaults',
     'files',
     'handlers',
     'library',
     'lookup_plugins',
     'meta',
     'module_utils',
     'tasks',
     'templates',
     'vars'
    ]

    """
     Create all the default directories in each new role and touch a main.yml file in each directory
     Some directories need the main.yml, but mostly this is to appease git so it doesn't skip directories without content.
    """

    for dirs in default_dirs:
        # https://gis.stackexchange.com/questions/107568/creating-multiple-folders-named-by-a-list-in-python
        # "Try casting the var as a string"
        d_default_dirs = os.path.join(role_dir,str(dirs))
        if not os.path.exists(d_default_dirs):
            os.mkdir(d_default_dirs)
    #        print("Directory", dirs, "created")
    #    else:
    #        print("Directory", dirs, "already exists")
        main_yml = f"{d_default_dirs}/main.yml"
        if not os.path.exists(main_yml):
            # https://stackoverflow.com/questions/6004073/how-can-i-create-directories-recursively
            Path(main_yml).touch()
    #        print("File", main_yml, "created")
    #    else:
    #        print("File", main_yml, "already exists")


    # Insert distro detection into role/tasks/main.yml and create {debian,rhel}.yml files

    task_dir = f"{role_dir}/tasks"
    distro_detect_main = f"{task_dir}/main.yml"

    if not os.path.exists(distro_detect_main):
        Path(distro_detect_main).touch()

    filename = distro_detect_main
    f = open(filename, "w")
    f.write("--- \n")
    f.write("\n")
    f.write("- include: debian.yml \n")
    f.write("  when: ansible_os_family == 'Debian'\n")
    f.write("\n")
    f.write("- include: rhel.yml\n")
    f.write("  when: ansible_os_family == 'RedHat'\n")
    f.write("\n")
    f.write("- include: manual.yml\n")
    f.write("\n")
    f.write("- include: common.yml\n")
    f.close()

    # Create a list of distro detection files

    distro_files = [ 'common.yml', 'debian.yml', 'rhel.yml', 'manual.yml' ]

    # Create all the distro_files files

    for files in distro_files:
        m_distro_files = os.path.join(task_dir,str(files))
        if not os.path.exists(m_distro_files):
            Path(m_distro_files).touch()

    # Molecule module variables

    molecule_dir = f"{role_dir}/molecule/default"
    molecule_tests_dir = f"{role_dir}/molecule/default/tests"
    molecule_dirs = f"{role_dir}/molecule/default/tests"
    os.makedirs(molecule_dirs, exist_ok=True)

    # Check if molecule_dir path exists - if not, create it

    if not os.path.exists(molecule_dir):
        os.mkdir(molecule_dir)

    # A multi-line list of all the files needed for the molecule module

    molecule_files = [
     'create.yml',
     'destroy.yml',
     'molecule.yml',
     'playbook.yml',
     'prepare.yml',
     'verifier.yml',
     'Dockerfile.j2',
     'INSTALL.rst'
     ]

    # A single line list containing all the molecule test files


    molecule_test_files = [ 'test_default.py', 'test_default.pyc' ]


    # Create all the molecule_files files


    for files in molecule_files:
        m_molecule_files = os.path.join(molecule_dir,str(files))
        if not os.path.exists(m_molecule_files):
            Path(m_molecule_files).touch()

    # Create all the molecule_test_files files

    for files in molecule_test_files:
        m_molecule_test_files = os.path.join(molecule_tests_dir,str(files))
        if not os.path.exists(m_molecule_test_files):
            Path(m_molecule_test_files).touch()

def delete_role():

    # these vars are in global vars
    #print(root_dir)
    #print(roles_dir)
    #print(role)
    # these are new vars for the function
    delete_role_file = Path(f"{root_dir}/{role}.yml")
    delete_role_dir = Path(f"{roles_dir}/{role}")
    #print(delete_role_file)
    #print(delete_role_dir)

    #is_file = Path(delete_role_file)
    #print(is_file)
    #is_dir = Path(delete_role_dir)
    #print(is_dir)

    if delete_role_file.exists():
    #if os.path.exists(delete_role_file):
        #delete_role_file.exists()
        #print("File exists:", delete_role_file)
        os.remove(delete_role_file)
    #else:
        #delete_role_file.exists()
        #print("File does not exist:", delete_role_file)

    #if delete_role_dir.is_file():
    if os.path.exists(delete_role_dir) and os.path.isdir(delete_role_dir):
    # https://stackoverflow.com/a/43771688
        #delete_role_dir.exists()
        # for some stupid reason this reports the directory exists every time when it doesn't
        #print("Directory exists:", delete_role_dir)
        shutil.rmtree(delete_role_dir)
        #print("Deleted directory:", delete_role_dir)
    #else:
        #delete_role_dir.exists()
        #print("Directory does not exist:", delete_role_dir)


if args.delete:
#if args.delete is not None:
    delete_role()
else:
    create_role()
