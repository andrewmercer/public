#!/usr/bin/env python

# https://pynative.com/python-random-choice

import random

pizza_places = [
 'Dynamite',
 'Dominos',
 'Little Caesars',
 'Liams Pizza',
 'Ginos'
]

more_pizza_places = [

 'Pyramid Pizza',
 'Pizza Hut',
 'Pizza Pizza'

]

# https://www.kite.com/python/answers/how-to-append-one-list-to-another-list-in-python
pizza_places.extend(more_pizza_places)

print(random.choice(pizza_places))
