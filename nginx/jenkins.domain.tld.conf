server {
  listen 80;
  server_name jenkins.domain.tld;
  return 301 https://$server_name$request_uri;
}

server {
  listen 443 default_server ssl;
  server_name jenkins.domain.tld;
  server_tokens off;

  ssl on;
  ssl_certificate /etc/nginx/ssl/wildcard.domain.tld;
  ssl_certificate_key /etc/nginx/ssl/wildcard.domain.tld;
  ssl_ciphers 'ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4';
  ssl_prefer_server_ciphers on;
  ssl_protocols  TLSv1 TLSv1.1 TLSv1.2;
  ssl_session_cache  shared:SSL:10m;
  ssl_session_timeout  5m;

  location / {
    proxy_pass http://jenkins.domain.tld:8080;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  }
}
